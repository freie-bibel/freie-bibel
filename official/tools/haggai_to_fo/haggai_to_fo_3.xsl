<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2012-2020  Stephan Kreutzer

This file is part of Freie Bibel.

Freie Bibel is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or
any later version, as published by the Free Software Foundation.

Freie Bibel is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with Freie Bibel. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" version="1.0" encoding="UTF-8"/>
  <!-- See http://xmlgraphics.apache.org/fop/trunk/fonts.html -->
  <xsl:param name="Schriftname">'FreeSans'</xsl:param>
  <xsl:param name="Schriftschnitt">normal</xsl:param>
  <xsl:param name="Schriftgroesse">10pt</xsl:param>
  <xsl:param name="Zeilenhoehe">12pt</xsl:param>
  <xsl:param name="Seitenbreite">148mm</xsl:param>
  <xsl:param name="Seitenhoehe">210mm</xsl:param>
  <xsl:param name="Rand-rechte-Seiten">30mm 30mm 30mm 30mm</xsl:param>
  <xsl:param name="Rand-linke-Seiten">30mm 30mm 30mm 30mm</xsl:param>
  <xsl:param name="Innenrand-rechte-Seiten">0mm 0mm 0mm 0mm</xsl:param>
  <xsl:param name="Innenrand-linke-Seiten">0mm 0mm 0mm 0mm</xsl:param>

  <xsl:template match="/">
    <fo:root>
      <fo:layout-master-set>
        <fo:simple-page-master page-height="{$Seitenhoehe}" page-width="{$Seitenbreite}" margin="{$Rand-rechte-Seiten}" master-name="Rechte Seite">
          <fo:region-body margin="{$Innenrand-rechte-Seiten}"/>
          <fo:region-before region-name="Kopf-rechte-Seiten" extent="{$Zeilenhoehe} * 2.2" display-align="before"/>
        </fo:simple-page-master>
        <fo:simple-page-master page-height="{$Seitenhoehe}" page-width="{$Seitenbreite}" margin="{$Rand-linke-Seiten}" master-name="Linke Seite">
          <fo:region-body margin="{$Innenrand-linke-Seiten}"/>
          <fo:region-before region-name="Kopf-linke-Seiten" extent="{$Zeilenhoehe} * 2.2" display-align="before"/>
        </fo:simple-page-master>
        <fo:page-sequence-master master-name="Inhalt-Seite">
          <fo:repeatable-page-master-alternatives>
            <fo:conditional-page-master-reference master-reference="Rechte Seite" odd-or-even="odd"/>
            <fo:conditional-page-master-reference master-reference="Linke Seite" odd-or-even="even"/>
          </fo:repeatable-page-master-alternatives>
        </fo:page-sequence-master>
      </fo:layout-master-set>
      <fo:page-sequence master-reference="Inhalt-Seite">
        <fo:static-content flow-name="Kopf-rechte-Seiten">
          <fo:block font-family="{$Schriftname}" font-size="{$Schriftgroesse}" line-height="{$Zeilenhoehe}" text-align="center" padding-before="({$Zeilenhoehe} * -1) - 2mm">
            <fo:retrieve-marker retrieve-class-name="Kapitel" retrieve-position="first-including-carryover" retrieve-boundary="page-sequence"/>
          </fo:block>
          <fo:block padding-before="-2.7mm">
            <fo:leader leader-length="100%" rule-thickness="0.5pt" leader-pattern="rule"/>
          </fo:block>
        </fo:static-content>
        <fo:static-content flow-name="Kopf-linke-Seiten">
          <fo:block font-family="{$Schriftname}" font-size="{$Schriftgroesse}" line-height="{$Zeilenhoehe}" text-align="center" padding-before="({$Zeilenhoehe} * -1) - 2mm">
            <fo:retrieve-marker retrieve-class-name="Kapitel" retrieve-position="first-including-carryover" retrieve-boundary="page-sequence"/>
          </fo:block>
          <fo:block padding-before="-2.7mm">
            <fo:leader leader-length="100%" rule-thickness="0.5pt" leader-pattern="rule"/>
          </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">
          <!-- fo:block font-family="{$Schriftname}" font-style="{$Schriftschnitt}" font-size="{$Schriftgroesse}" line-height="{$Zeilenhoehe}" hyphenate="true" lang="de" -->
          <xsl:apply-templates select="./XMLBIBLE/BIBLEBOOK"/>
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>

  <xsl:template match="BIBLEBOOK">
    <fo:block font-family="{$Schriftname}" font-style="{$Schriftschnitt}" font-size="{$Schriftgroesse}" line-height="{$Zeilenhoehe}" page-break-before="always">
      <xsl:apply-templates select="./CAPTION"/>
      <xsl:apply-templates select="./CHAPTER"/>
    </fo:block>
  </xsl:template>
  <xsl:template match="/XMLBIBLE/BIBLEBOOK/CAPTION">
    <fo:block font-size="{$Schriftgroesse} * 1.4" line-height="{$Schriftgroesse} * 1.3" font-weight="bold" text-align="center" space-after="6mm" space-after.precedence="1" keep-with-next.within-page="always">
      <xsl:value-of select="./text()"/>
    </fo:block>
  </xsl:template>
  <xsl:template match="/XMLBIBLE/BIBLEBOOK/CHAPTER">
    <fo:block keep-with-next.within-page="always">
      <fo:marker marker-class-name="Kapitel">
        <xsl:value-of select="../@bname"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="@cnumber"/>
      </fo:marker>
    </fo:block>
    <xsl:apply-templates select="./PARAGRAPH"/>
  </xsl:template>
  <xsl:template match="PARAGRAPH">
    <fo:block text-align="justify">
      <xsl:if test="count(preceding-sibling::PARAGRAPH) = 0">
        <fo:block>
          <fo:float float="start">
            <fo:block line-stacking-strategy="font-height" font-size="200%" margin="-8pt" padding-left="5pt" padding-top="5pt" padding-right="2pt">
              <fo:block padding-top="{$Schriftgroesse} * 0.165" font-weight="bold">
                <xsl:value-of select="../@cnumber"/>
              </fo:block>
            </fo:block>
          </fo:float>
        </fo:block>
        <!--fo:inline font-size="200%">
          <xsl:value-of select="../@cnumber"/>
        </fo:inline>
        <xsl:text>&#x0020;</xsl:text-->
      </xsl:if>
      <xsl:apply-templates select="VERSE"/>
    </fo:block>
  </xsl:template>
  <xsl:template match="VERSE">
    <xsl:if test="not(@vnumber = 1)">
      <fo:inline font-weight="bold">
        <xsl:value-of select="@vnumber"/>
      </fo:inline>
      <xsl:text>&#x00A0;</xsl:text>
    </xsl:if>
    <xsl:apply-templates/>
    <xsl:text>&#x0020;</xsl:text>
  </xsl:template>
  <xsl:template match="VERSE/text()">
    <fo:inline font-style="{$Schriftschnitt}">
      <xsl:value-of select="."/>
    </fo:inline>
  </xsl:template>
  <xsl:template match="STYLE">
    <xsl:choose>
      <xsl:when test="@fs='super'">
        <xsl:text>[</xsl:text><xsl:apply-templates/><xsl:text>]</xsl:text>
      </xsl:when>
      <xsl:when test="@fs='emphasis'">
        <fo:inline font-weight="bold">
          <xsl:apply-templates/>
        </fo:inline>
      </xsl:when>
      <xsl:when test="@fs='italic'">
        <fo:inline font-style="italic">
          <xsl:apply-templates/>
        </fo:inline>
      </xsl:when>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="STYLE/text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="node()|@*|text()"/>

</xsl:stylesheet>
